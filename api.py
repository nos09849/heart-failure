import sys
import joblib
import logging

import pandas as pd

from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional, List, Dict


MODELS = {
    "Tree": joblib.load("tree.joblib"),
    "LogisticRegression": joblib.load("log_reg.joblib"),
    "SVM": joblib.load("svm.joblib"),
}

FEATURE_ORDER = ['age', 'anaemia', 'creatinine_phosphokinase', 'diabetes', 'ejection_fraction',
    'high_blood_pressure', 'platelets', 'serum_creatinine', 'serum_sodium', 'sex', 'smoking',
    'time'
]


logging.basicConfig(stream=sys.stdout, level=logging.INFO)


class PredictRequest(BaseModel):
    features: Dict[str, float]


class ModelResponse(BaseModel):
    predictions: Optional[Dict[str, str]]
    error: Optional[str]


app = FastAPI()

@app.get("/", response_model=ModelResponse)
async def root() -> ModelResponse:
    return ModelResponse(error="This is a test endpoint.")


@app.get("/predict", response_model=ModelResponse)
async def explain_api() -> ModelResponse:
    return ModelResponse(
        error="Send a POST request to this endpoint with 'features' data."
    )


@app.post("/predict")
async def get_model_predictions(request: PredictRequest) -> ModelResponse:
    feature_dict = request.features
    logging.info(feature_dict)

    for feat_name in FEATURE_ORDER:
        if feat_name not in feature_dict:
            return ModelResponse(error=f"Missing feature: '{feat_name}'")

    pred = {}
    df = pd.DataFrame.from_records([feature_dict])
    for name, model in MODELS.items():
        pred[name] = "dies" if model.predict(df)[0] == 1 else "survives"

    return ModelResponse(predictions=pred)
