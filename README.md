# Analysis

Very short analysis of the dataset is in `analysis.html`. I think that the most
interesting part is that the final trained decision tree has only 3 levels and
the most important features are:
* time
* ejection fraction
* serum creatinine
If I had more time, I'd investigate this more and maybe do more a bit of feature
engineering.

# Training script

```
python train_model.py
```
This script saves the models to local files after training.

# Model serving

```
uvicorn api:app
```

The REST API takes one sample with features and returns a prediction for each
classifier. The easiest way to aggregate over the classifier predictions is to use majority
vote. I trained every classifier separately because that was the easiest option
to implement for me. There are also techniques like bagging and boosting that
might increase the performance.

Example request:
```
-> % curl -X POST localhost:8000/predict --data '{"features": {"age": 65.0, "anaemia": 0, "creatinine_phosphokinase": 167, "diabetes": 0, "ejection_fraction": 30, "high_blood_pressure": 0, "platelets": 259000.0, "serum_creatinine": 0.8, "serum_sodium": 138, "sex": 0, "smoking": 0, "time": 186}}'
{"predictions":{"Tree":"survives","LogisticRegression":"survives","SVM":"survives"},"error":null}%
```

Incorrect request:
```
-> % curl -X POST localhost:8000/predict --data '{"features": {"a": 4}}'
{"predictions":null,"error":"Missing feature: 'age'"}%
```
